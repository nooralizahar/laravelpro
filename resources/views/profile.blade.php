@extends('layouts.app')
@php
if (isset($row)) {
    $category_id  = $row->category_id;
} else {
    $category_id  = "-1";
} 
@endphp
@section('content')
<div class="page-wrapper">
    <!-- Page-body start -->
    <div class="page-body">
        <div class="row">
            <div class="col-md-12">
                <div class="card table-card">
                    <div class="card-header">
                        <h5>Profile</h5>
                    </div>
                    <div class="card-block">
                        <div class="card col-md-12">
                            <div class="card-body">
                                @if (session('error'))
                                <div class="alert alert-danger"><i class="mdi mdi-exclamation text-danger"></i> {{ session('error') }}</div>
                                @elseif (session('success'))
                                <div class="alert alert-success"><i class="mdi mdi-exclamation text-success"></i> {{ session('success') }}</div>
                                @endif
                                <form class="forms-sample" method="POST" action="/users/update" enctype="multipart/form-data">
                                    @csrf
                                    <input type="hidden" name="id" id="" value="{{ isset($row) ? $row->id : '' }}">
                                    <div class="form-group">
                                        <label for="exampleInputName1">Name</label>
                                        <input type="text" name="name" class="form-control" id="exampleInputName1" placeholder="name" value="{{ isset($row) ? $row->name : '' }}" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputName1">Email</label>
                                        <input type="email" name="email" class="form-control" id="exampleInputName1" placeholder="email" value="{{ isset($row) ? $row->email : '' }}" readonly>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputName1">Phone Number</label>
                                        <input type="number" name="phone_number" class="form-control" id="exampleInputName1" placeholder="phone_number" value="{{ isset($row) ? $row->phone_number : '' }}" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputName1">Password</label>
                                        <input type="password" name="password" class="form-control" id="exampleInputName1" placeholder="password" value="">
                                        <input type="hidden" name="old_password" id="" value="{{ isset($row) ? $row->password : '' }}">
                                        <small class="text-danger">* Kosongkan jika tidak diupdate</small>
                                    </div>
                                    <button type="submit" class="btn btn-gradient-primary mr-2">Update</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--  project and team member end -->
        </div>
        
    </div>
    
    <!-- Page-body end -->
</div>
<div id="styleSelector"> </div>
@endsection
@section('js')

@endsection
