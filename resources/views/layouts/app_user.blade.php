<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>Tychica</title>
    <link rel="stylesheet" href="/frontend/libraries/bootstrap/css/bootstrap.css" />
    <link href="https://fonts.googleapis.com/css2?family=Assistant:wght@200;300;400;500;600;700;800&family=Playfair+Display:ital,wght@0,400;0,500;0,600;0,700;0,800;0,900;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="/frontend/styles/main.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css">
</head>
<body>
    <!--Navbar-->
    <div class="container">
        <nav class="row navbar navbar-expand-lg navbar-light bg-white">
            <a href="#" class="navbar-brand">
                <img src="/frontend/images/logo-ty.png" alt="Logo TYCHICA" />
            </a>
            <button class="navbar-toggler navbar-toggler-right"
            type="button"
            data-toggle="collapse"
            data-target="#navb"
            >
            <span class="navbar-toggler-icon"></span>
        </button>
        
        <div class="collapse navbar-collapse" id="navb">
            <ul class="navbar-nav ml-auto mr-3">
                <li class="nav-item mx-md-2"><a href="/home" class="nav-link">Home</a></li>
                <li class="nav-item mx-md-2"><a href="/purchases" class="nav-link">Purchase</a></li>
                @guest
                <li class="nav-item mx-md-2"><a href="/login" class="nav-link">Login</a></li>
                <li class="nav-item mx-md-2"><a href="/register" class="nav-link">Register</a></li>
                @else
                <li class="nav-item mx-md-2"><a href="#" class="nav-link active">{{ Auth::user()->name }}</a></li>
                <li class="nav-item mx-md-2">
                    <a class="nav-link text-danger" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        Logout
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                        @csrf
                    </form>
                </li>
                @endif
            </ul>
            
        </div>
    </nav>
</div>

<!--Header-->
<header class="header text-center">
    <h1>
        Welcome Tychicaers !
    </h1>
    <p>
        YOUR PREMIUM APP STORE 
    </p>
</header>

<main>
    @yield('content')
</main>

<!--Footer-->
<footer class="section-footer mt-5 mb-4 border-top">
    <div class="container pt-5 pb-5">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="row">
                    
                    <div class="col-12 col-lg-3">
                        <h5>ABOUT</h5>
                        <ul class="list-unstyled">
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Sequi harum error autem molestias fugiat dolore veniam natus, sapiente voluptas ipsum, dolores eius in? Eos laborum illo inventore, odit a ipsum.</p>
                        </ul>
                    </div>
                    
                    <div class="col-12 col-lg-3">
                        <h5>SOCIAL MEDIA</h5>
                        <ul class="list-unstyled">
                            <li><a class="social-media-icon" href="https://instagram.com/"><span class="fab fa-instagram"></span> tychica_app</a></li>
                            <a class="social-media-icon" href="https://twitter.com/"><span class="fab fa-twitter"></span> tychica</a>
                        </ul>
                    </div>
                    
                    <div class="col-12 col-lg-3">
                        <h5>GET CONNECTED</h5>
                        <ul class="list-unstyled">
                            <li>Depok, Jawa Barat</li>
                            <li>Indonesia</li>
                            <li>tychica@apps.id</li>
                        </ul>
                    </div>

                    
                    <div class="col-12 col-lg-3">
                        <h5>Contact Us</h5>
                        <ul class="list-unstyled">
                            <form action="" class="row">
                                <div class="form-group">
                                    <input type="email" name="" id="" class="form-control" placeholder="Email....">
                                </div>
                                <div class="form-group">
                                    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="Messages..."></textarea>
                                </div>
                            </form>
                        </ul>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row border-top justify-content-center align-item-center pt-4">
            <div class="col-auto text-grey-500 font-weight-light" id="copyright">
                © 2020 Nomads ● Alright Reserved 
            </div>
        </div>
    </div>
</footer>
<script src="/frontend/libraries/jquery/jquery-3.6.0.min.js"></script>
<script src="/frontend/libraries/bootstrap/js/bootstrap.js"></script>
<script src="/frontend/libraries/retina/retina.min.js"></script>
</body>
</html>