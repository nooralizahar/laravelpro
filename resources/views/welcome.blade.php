@extends('layouts.app_user')

@section('content')
<div class="container">
    <section class="section-stats row justify-content-center" id="stats">
        <div class="col-3 col-md-2 stats-detail">
            <h2>{{ $streaming ?? '0' }}</h2>
            <p>Streaming Apps</p>
        </div>  
        <div class="col-3 col-md-2 stats-detail">
            <h2>{{ $music ?? '0' }}</h2>
            <p>Music Apps</p>
        </div>  
        <div class="col-3 col-md-2 stats-detail">
            <h2>{{ $editing ?? '0' }}</h2>
            <p>Editing Apps</p>
        </div> 
    </section>
</div>
<section class="section-popular" id="popular">
    <div class="container">
        <div class="row">
            <div class="col text-center section-popular-heading">
                <h2>Apps Premium Popular</h2>
            </div>
        </div>
    </div>
</section>

<section class="section-popular-content" id="popular content">
    <div class="container">
        <div class="section-popular-app row justify-content-center">
            
            @foreach ($apps as $item)
            <div class="col-sm-6 col-md-4 col-lg-3">
                <div class="card-app text-center d-flex flex-column">
                    <img src="/image/{{ $item->image ?? '' }}">
                    <div class="app">{{ $item->name ?? '' }}</div>
                    <div class="app-button mt-auto">
                        <a href="/detail/{{ $item->id }}" class="btn btn-app-details px-4">
                            View Details
                        </a>   
                    </div>                                   
                </div>
            </div>
            @endforeach

        </div>
    </div>
</section>

<section class="section-pay" id="payment">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <h2>Our Payment</h2>
                <p>
                    We are trusted  
                </p>
            </div>
            
            <div class="col-md-8 text-center">
                <img src="frontend/images/pay.png" alt="logo partner" class="img-pay" />
            </div>
        </div>
    </div>
</section>
@endsection
