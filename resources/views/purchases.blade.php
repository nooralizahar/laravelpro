@extends('layouts.app_user')

@section('content')
<div class="container">
    <section class="section-stats row justify-content-center" id="stats">
        <div class="col-3 col-md-2 stats-detail">
            <h2>{{ $streaming ?? '0' }}</h2>
            <p>Streaming Apps</p>
        </div>  
        <div class="col-3 col-md-2 stats-detail">
            <h2>{{ $music ?? '0' }}</h2>
            <p>Music Apps</p>
        </div>  
        <div class="col-3 col-md-2 stats-detail">
            <h2>{{ $editing ?? '0' }}</h2>
            <p>Editing Apps</p>
        </div> 
    </section>
</div>
<section class="section-popular" id="popular">
    <div class="container">
        <div class="row">
            <div class="col text-center section-popular-heading">
                <h2>Purchases Apps</h2>
            </div>
        </div>
    </div>
</section>

<section class="section-popular-content" id="popular content">
    <div class="container">
        <div class="section-popular-app row justify-content-center">
            
            @foreach ($purchases as $item)
            <div class="col-sm-6 col-md-4 col-lg-3">
                <div class="card text-center rounded" style="background-color: rgb(240, 241, 248)">
                    <div class="card-body">
                        <div class="app"><b>{{ $item->apps_name->name ?? '' }}</b></div>
                        <br>
                        <div class="text-dark text-left">
                            Date: <b>{{ date('d M Y', strtotime($item->purchase_date)) }}</b><br>
                            Time: <b>{{ $item->purchase_time }} Month</b><br>
                            Price: <b>Rp. {{ number_format($item->price_total,2,',','.'); }}</b>
                        </div>
                        <br>
                        <div class="app-button mt-auto">
                            @if ($item->status_payment == 0)
                            @php
                                $item = '
                                Kepada Admin TYCHIHA, saya atas nama '.$item->users_name->name.' telah melakukan pembelian Apps '.$item->apps_name->name.' pada tanggal '.date('d M Y', strtotime($item->purchase_date)).' dengan jumlah waktu '.$item->purchase_time.' bulan dengan total harga Rp. '.number_format($item->price_total,2,',','.').'.
                                Berikut saya lampirkan bukti pembayarannya.
                                '
                            @endphp
                            <a href="https://api.whatsapp.com/send?phone=6282138699795&text={{ urlencode($item) }}" data-action="share/whatsapp/share" target="_blank" class="btn btn-success">Whatsapp Confirmation</a>
                            @elseif($item->status_payment == 2)
                            <button class="btn btn-danger">Payment Rejected</button>
                            @else
                            <button class="btn btn-primary">Payment Confirmed</button>
                            @endif
                        </div>            
                    </div>                       
                </div>
            </div>
            @endforeach
            
        </div>
    </div>
</section>

<section class="section-pay" id="payment">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <h2>Payment Method</h2>
            </div>
            
            <div class="col-md-8 text-center">
                <img src="frontend/images/pay.png" alt="logo partner" class="img-pay" />
            </div>
        </div>
    </div>
</section>
@endsection
