@extends('layouts.app')

@section('content')
<div class="page-wrapper">
    <!-- Page-body start -->
    <div class="page-body">
        <div class="row">
            <div class="col-md-12">
                <div class="card table-card">
                    <div class="card-header">
                        <h5>Apps</h5>
                        <nav aria-label="" style="float: right">
                            <a href="/apps/form" class="btn btn-primary btn-sm"><span class="mdi mdi-plus"></span> + Add Data</a>
                        </nav>
                    </div>
                    <div class="card-block">
                        <div class="card-body table-responsive">
                            @if (session('error'))
                            <div class="alert alert-danger"><i class="mdi mdi-exclamation text-danger"></i> {{ session('error') }}</div>
                            @elseif (session('success'))
                            <div class="alert alert-success"><i class="mdi mdi-exclamation text-success"></i> {{ session('success') }}</div>
                            @endif
                            <table class="table table-bordered datatable" id="table" style="100%">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Title</th>
                                        <th>Image</th>
                                        <th>Category</th>
                                        <th>Price</th>
                                        <th>Update At</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!--  project and team member end -->
        </div>
        
    </div>
    
    <!-- Page-body end -->
</div>
<div id="styleSelector"> </div>
@endsection
@section('js')
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript">
    jQuery(function($) {
        
        $('#table').DataTable({
            processing: true,
            serverSide: true,
            autoWidth: false,
            ajax: "{{ route('apps-data') }}",
            columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex'},
            {data: 'name', name: 'name'},
            {data: 'image', name: 'image'},
            {data: 'category_name', name: 'category_name'},
            {data: 'price', name: 'price'},
            {data: 'updated_at', name: 'updated_at'},
            {
                data: 'action', 
                name: 'action', 
                orderable: true, 
                searchable: true
            },
            ],
                        columnDefs: [{
                            targets: [1,2]
                        }]
        });
        
    });
</script>
@endsection
