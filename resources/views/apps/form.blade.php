@extends('layouts.app')
@php
if (isset($row)) {
    $category_id  = $row->category_id;
} else {
    $category_id  = "-1";
} 
@endphp
@section('content')
<div class="page-wrapper">
    <!-- Page-body start -->
    <div class="page-body">
        <div class="row">
            <div class="col-md-12">
                <div class="card table-card">
                    <div class="card-header">
                        <h5>Apps</h5>
                    </div>
                    <div class="card-block">
                        <div class="card col-md-12">
                            <div class="card-body">
                                <h4 class="card-title mb-4">Form</h4>
                                @if (session('error'))
                                <div class="alert alert-danger"><i class="mdi mdi-exclamation text-danger"></i> {{ session('error') }}</div>
                                @elseif (session('success'))
                                <div class="alert alert-success"><i class="mdi mdi-exclamation text-success"></i> {{ session('success') }}</div>
                                @endif
                                <form class="forms-sample" method="POST" action="/apps/save/{{ isset($row) ? $row->id : '' }}" enctype="multipart/form-data">
                                    @csrf
                                    <input type="hidden" name="id" id="" value="{{ isset($row) ? $row->id : '' }}">
                                    <div class="form-group">
                                        <label for="exampleInputName1">Title</label>
                                        <input type="text" name="name" class="form-control" id="exampleInputName1" placeholder="name" value="{{ isset($row) ? $row->name : '' }}" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputName1">Price</label>
                                        <input type="number" name="price" class="form-control" id="exampleInputName1" placeholder="price" value="{{ isset($row) ? $row->price : '' }}" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Image</label>
                                        <img class="img-fluid" src="/image/{{ isset($row) ? $row->image : '' }}" alt="" style="width: 200px">
                                        <div class="input-group col-xs-12">
                                            <input type="file" class="form-control file-upload-info" name="image" placeholder="Upload Image">
                                            <input type="hidden" name="old_image" id="" value="{{ isset($row) ? $row->image : '' }}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputName1">Category Apps</label>
                                        <select name="category_name" id="" required class="form-control">
                                            <option value="" selected>Select Category</option>
                                            <option value="Streaming Apps" {{ isset($row) ? ($row->title == 'Streaming Apps' ? 'selected' : '') : '' }}>Streaming Apps</option>
                                            <option value="Music Apps" {{ isset($row) ? ($row->title == 'Music Apps' ? 'selected' : '') : '' }}>Music Apps</option>
                                            <option value="Editing Apps" {{ isset($row) ? ($row->title == 'Editing Apps' ? 'selected' : '') : '' }}>Editing Apps</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleTextarea1">Description</label>
                                        <textarea class="form-control description" name="description" id="exampleTextarea1" rows="4" required>{{ isset($row) ? $row->description : '' }}</textarea>
                                    </div>
                                    <button type="submit" class="btn btn-gradient-primary mr-2">Submit</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--  project and team member end -->
        </div>
        
    </div>
    
    <!-- Page-body end -->
</div>
<div id="styleSelector"> </div>
@endsection
@section('js')

@endsection
