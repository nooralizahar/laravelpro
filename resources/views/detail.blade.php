@extends('layouts.app_user')
@section('content')
<section class="section-details-header"></section>
<section class="section-details-content">
    <div class="container">
        <div class="row">
            <div class="col p-0">
                <nav>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-detail">
                            Apps 
                        </li>
                        <li class="breadcrumb-detail">
                            /
                        </li>
                        <li class="breadcrumb-item active">
                            Details 
                        </li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="card card-details">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="gallery">
                                    <div class="xzoom-container">
                                        <img src="/image/{{ $detail->image ?? '' }}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-8">
                                <h1><b>{{ $detail->name ?? '' }}</b></h1>
                                <h6>
                                    <b>Rp. {{ number_format($detail->price,2,',','.'); }}</b>
                                </h6>
                                <h6>
                                    {{ $detail->description ?? '' }}
                                </h6>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer bg-white" style="text-align: right">
                        <a href="/" class="btn btn-warning mr-3">
                            Back
                        </a>
                        <button class="btn btn-primary" type="button" data-toggle="modal" data-target="#exampleModal">
                            Purchase
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <form class="forms-sample" method="POST" action="/purchases/store" enctype="multipart/form-data">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Purchase</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    @csrf
                    <input type="hidden" name="apps_id" id="" value="{{ $detail->id ?? '' }}">
                    <div class="form-group">
                        <label for="exampleInputName1">Number Of Month</label>
                        <input type="number" min="1" name="purchase_time" class="form-control" id="exampleInputName1" placeholder="Purchase Time" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Purchase</button>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection
