<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Apps extends Model
{
    use HasFactory;
    protected $table = 'apps';
    protected $fillable = ['name', 'image', 'description', 'price', 'category_name'];

    public function purchases()
    {
        return $this->hasMany(Purchases::class, 'apps_id', 'id');
    }
}
