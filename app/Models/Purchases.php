<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Purchases extends Model
{
    use HasFactory;
    protected $table = 'purchases';
    protected $fillable = ['users_id', 'apps_id', 'purchase_date', 'purchase_time', 'price_total', 'status_payment'];

    public function users_name(){
        return $this->belongsTo(User::class, 'users_id', 'id');
    }

    public function apps_name(){
        return $this->belongsTo(Apps::class, 'apps_id', 'id');
    }
}
