<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use DB;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        return view('users');
    }
    
    public function data()
    {
        $query = User::where('roles', 2)->orderBy('id', 'DESC')->get();
        
        return DataTables::of($query)->addIndexColumn()
        ->editColumn('created_at', function($data){
            $detail = date("d M Y H:i", strtotime($data->created_at));
            return $detail;
        })
        ->rawColumns([])
        ->make(true);
        
    }
    
    public function profile()
    {
        $data['row'] = User::where('id', Auth::user()->id)->first();
        return view('profile', $data);
    }
    
    public function update(Request $request)
    {
        if (isset($request->password)) {
            $password = Hash::make($request->password);
        } else {
            $password = $request->old_password;
        }
        
        $post = User::find($request->id);
        
        $post->name = $request->name;
        $post->email = $request->email;
        $post->phone_number = $request->phone_number;
        $post->password = $password;
        
        $post->save();

        return redirect()->route('users-profile')->with(['success' => 'Update Success']);
    }
}
