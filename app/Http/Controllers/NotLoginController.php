<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Apps;

class NotLoginController extends Controller
{

    public function index()
    {
        $data['apps'] = Apps::get();
        $data['streaming'] = Apps::where('category_name', 'Streaming Apps')->count();
        $data['music'] = Apps::where('category_name', 'Music Apps')->count();
        $data['editing'] = Apps::where('category_name', 'Editing Apps')->count();
        return view('welcome', $data);
    }

    public function detail($id = null)
    {
        $data['detail'] = Apps::where('id', $id)->first();
        return view('detail', $data);
    }
}
