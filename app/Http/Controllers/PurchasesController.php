<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Facades\Auth;
use App\Models\Purchases;
use DB;

class PurchasesController extends Controller
{
    /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        return view('purchases.index');
    }
    
    public function data()
    {
        $query = Purchases::orderBy('id', 'DESC')->get();
        
        return DataTables::of($query)->addIndexColumn()
        ->editColumn('updated_at', function($data){
            $detail = date("d M Y H:i", strtotime($data->updated_at));
            return $detail;
        })->editColumn('purchase_date', function($data){
            $detail = date("d M Y", strtotime($data->purchase_date));
            return $detail;
        })->editColumn('price_total', function($data){
            $detail = "Rp " . number_format($data->price_total,2,',','.');
            return $detail;
        })->editColumn('users_id', function($data){
            $detail = $data->users_name->name ?? '-';
            return $detail;
        })->editColumn('status_payment', function($data){
            if ($data->status_payment == 0) {
                $detail = 'waiting payment';
            } elseif($data->status_payment == 2){
                $detail = 'rejected payment';
            } else {
                $detail = 'confirmed payment';
            }
            return $detail;
        })->editColumn('apps_id', function($data){
            $detail = $data->apps_name->name ?? '-';
            return $detail;
        })->addColumn('action', function($data){
            if ($data->status_payment == 0) {
                $detail = '<a href="'.route("purchase-confirm", $data->id).'" class="confirm text-success" style="text-decoration: none"><i class="mdi mdi-confirm icon-sm"></i> Confirm</a>
                | <a href="'.route("purchase-reject", $data->id).'" class="reject text-danger" style="text-decoration: none"><i class="mdi mdi-reject icon-sm"></i> Reject</a>
                <script src="/js/alert.js"></script>';
            } else {
                $detail = '-';
            }
            return $detail;
        })
        ->rawColumns(['action'])
        ->make(true);
        
    }
    
    public function confirm($id = null)
    {
        $post = Purchases::find($id);
        $post->status_payment = 1;
        $post->save();
        
        return redirect()->route('purchase')->with(['success' => 'Confirm Success']);
    }
    
    public function reject($id = null)
    {
        $post = Purchases::find($id);
        $post->status_payment = 2;
        $post->save();
        
        return redirect()->route('purchase')->with(['success' => 'Reject Success']);
    }
}
