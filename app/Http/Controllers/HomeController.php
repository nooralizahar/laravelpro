<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Apps;
use App\Models\Purchases;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if (Auth::user()->roles == 1) {
            return view('dashboard');
        } else {
            $data['apps'] = Apps::get();
            $data['streaming'] = Apps::where('category_name', 'Streaming Apps')->count();
            $data['music'] = Apps::where('category_name', 'Music Apps')->count();
            $data['editing'] = Apps::where('category_name', 'Editing Apps')->count();
            return view('index', $data);
        }
    }

    public function purchases()
    {
        $data['purchases'] = Purchases::where('users_id', Auth::user()->id)->orderBy('id', 'DESC')->get();
        $data['streaming'] = Apps::where('category_name', 'Streaming Apps')->count();
        $data['music'] = Apps::where('category_name', 'Music Apps')->count();
        $data['editing'] = Apps::where('category_name', 'Editing Apps')->count();
        return view('purchases', $data);
    }

    public function purchases_store(Request $request)
    {
        $get_apps = Apps::where('id', $request->apps_id)->first();
        $total = (int)$get_apps->price * (int)$request->purchase_time;
        $post = Purchases::create([
            'users_id' => Auth::user()->id,
            'apps_id' => $request->apps_id,
            'purchase_time' => $request->purchase_time,
            'purchase_date' => date('Y-m-d'),
            'price_total' => $total
        ]);

        return redirect()->route('purchases')->with(['success' => 'Purchases Success']);
    }
}
