<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Facades\Auth;
use App\Models\Apps;
use DB;
use Illuminate\Support\Str;

class AppsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('apps.index');
    }

    public function data()
    {
        $query = Apps::orderBy('id', 'DESC')->get();

        return DataTables::of($query)->addIndexColumn()
        ->editColumn('updated_at', function($data){
            $detail = date("d M Y H:i", strtotime($data->updated_at));
            return $detail;
        })->editColumn('image', function($data){
            $detail = '<img class="img-fluid" src="/image/'.$data->image.'" width="50px">';
            return $detail;
        })->editColumn('price', function($data){
            $detail = "Rp " . number_format($data->price,2,',','.');
            return $detail;
        })->editColumn('description', function($data){
            $text = strip_tags($data->description);
            $detail = Str::limit($text, 50);
            return $detail;
        })->addColumn('action', function($data){
            $detail = '<a class="text-info" href="'.route("apps-form", $data->id).'" style="text-decoration: none"><i class="mdi mdi-pencil icon-sm"></i> Edit</a> | 
            <a href="'.route("apps-delete", $data->id).'" class="delete text-danger" style="text-decoration: none"><i class="mdi mdi-delete icon-sm"></i> Delete</a>
            <script src="/js/alert.js"></script>';
            return $detail;
        })
        ->rawColumns(['image', 'action', 'description'])
        ->make(true);

    }

    public function form($id = null)
    {
        $data['row'] = Apps::where('id', $id)->first();
        return view('apps.form', $data);
    }
    
    public function save(Request $request, $id = null)
    {
        if(isset($id)) {
            if ($request->image != null) {
                $file = $request->file('image');
                $fileName = date('YmdHi').'_'.$file->getClientOriginalName();
                $file->move(public_path('image'), $fileName);

                $old_file_name = $request->old_image;
                $path=public_path().'/image'.$old_file_name;

                if (isset($old_file_name)) {
                    if (file_exists($path)) {
                        unlink($path);
                    }
                }

            } else {
                $fileName = $request->old_image;
            }

            $post = Apps::find($id);

            $post->name = $request->name;
            $post->image = $fileName;
            $post->description = $request->description;
            $post->category_name = $request->category_name;
            $post->price = $request->price;

            $post->save();

            return redirect()->route('apps')->with(['success' => 'Update Success']);

        } else {

            $file = $request->file('image');
            $fileName = date('YmdHi').'_'.$file->getClientOriginalName();
            $file->move(public_path('image'), $fileName);

            $post = Apps::create([
                'name' => $request->name,
                'image' => $fileName,
                'description' => $request->description,
                'category_name' => $request->category_name,
                'price' => $request->price,
            ]);

            return redirect()->route('apps')->with(['success' => 'Upload Success']);

        }
    }

    public function delete($id = null)
    {
        $data = Apps::find($id);
        $file = Apps::where('id', $id)->first();

        $old_file_name = $file->image;
        $path=public_path().'/image/'.$old_file_name;

        if (!empty($file->image)) {
            if (file_exists($path)) {
                unlink($path);
            }
        }
        $data->delete();
        return redirect()->route('apps')->with(['success' => 'Delete Success']);
    }
}
