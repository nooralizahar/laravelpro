<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', [App\Http\Controllers\NotLoginController::class, 'index']);
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/detail/{id?}', [App\Http\Controllers\NotLoginController::class, 'detail'])->name('detail');
Route::get('/purchases', [App\Http\Controllers\HomeController::class, 'purchases'])->name('purchases');
Route::post('/purchases/store', [App\Http\Controllers\HomeController::class, 'purchases_store'])->name('purchases-store');

Route::get('/apps', [App\Http\Controllers\AppsController::class, 'index'])->name('apps');
Route::get('/apps/data', [App\Http\Controllers\AppsController::class, 'data'])->name('apps-data');
Route::get('/apps/form/{id?}', [App\Http\Controllers\AppsController::class, 'form'])->name('apps-form');
Route::post('/apps/save/{id?}', [App\Http\Controllers\AppsController::class, 'save'])->name('apps-save');
Route::get('/apps/delete/{id?}', [App\Http\Controllers\AppsController::class, 'delete'])->name('apps-delete');

Route::get('/purchase', [App\Http\Controllers\PurchasesController::class, 'index'])->name('purchase');
Route::get('/purchase/data', [App\Http\Controllers\PurchasesController::class, 'data'])->name('purchase-data');
Route::get('/purchase/confirm/{id?}', [App\Http\Controllers\PurchasesController::class, 'confirm'])->name('purchase-confirm');
Route::get('/purchase/reject/{id?}', [App\Http\Controllers\PurchasesController::class, 'reject'])->name('purchase-reject');

Route::get('/users', [App\Http\Controllers\UserController::class, 'index'])->name('users');
Route::get('/users/data', [App\Http\Controllers\UserController::class, 'data'])->name('users-data');
Route::get('/profile', [App\Http\Controllers\UserController::class, 'profile'])->name('users-profile');
Route::post('/users/update', [App\Http\Controllers\UserController::class, 'update'])->name('users-update');
